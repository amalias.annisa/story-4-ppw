from django.shortcuts import render


def home(request):
    return render(request, 'main/story3.html')

def korea(request):
    return render(request, 'main/story3-2.html')