from django import forms


class KegiatanForm(forms.Form):
    nama_kegiatan = forms.CharField(label='Activity name', max_length=50,
                                    widget=forms.TextInput(attrs={'class': 'form-control'}))


class PesertaForm(forms.Form):
    nama_peserta = forms.CharField(label='Write your name below :', max_length=64,
                                   widget=forms.TextInput(attrs={'class': 'form-control'}))
