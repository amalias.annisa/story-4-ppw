from django.test import TestCase

# Create your tests here.

from django.test import Client
from django.urls import resolve

from story6.models import Kegiatan, Peserta
from . import views

class UnitTestForStory6(TestCase):
    def test_main_page(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_template_story6(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6.html')

    def test_function_page(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, views.story6)

    def test_text_on_activities(self):
        response = Client().get('/story6/')
        content = response.content.decode('utf8')
        self.assertIn("My Activity List", content)        
        self.assertIn("Activity name:", content)
        self.assertIn("Add activity", content)

    def test_story6_add_new_activity(self):
        Client().post('/story6/post-activity', data={'nama_kegiatan': 'minum coklat panas'})
        jumlah = Kegiatan.objects.filter(nama='minum coklat panas').count()
        self.assertEqual(jumlah, 1)

    def test_story6_add_new_activity_invalid(self):
        Client().post('/story6/post-activity', data={})
        jumlah = Kegiatan.objects.filter(nama='bermain tictactoe').count()
        self.assertEqual(jumlah, 0)

    def test_story6_add_new_participant(self):
        obj = Kegiatan.objects.create(nama='Unit Test2')
        Client().post('/story6/post-participant/' + str(obj.id), data={'nama_peserta': 'chc'})
        jumlah = Peserta.objects.filter(nama='chc').count()
        self.assertEqual(jumlah, 1)

    def test_story6_add_new_participant_invalid(self):
        Client().post('/story6/post-participant', data={})
        jumlah = Peserta.objects.filter(nama='Spongebob').count()
        self.assertEqual(jumlah, 0)

    def test_story6_model_kegiatan(self):
        Kegiatan.objects.create(nama='ngerjain ppw')
        kegiatan = Kegiatan.objects.get(nama='ngerjain ppw')
        self.assertEqual(str(kegiatan), 'ngerjain ppw')