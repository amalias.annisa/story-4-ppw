from django.urls import path

from . import views

urlpatterns = [
    path('', views.story6),
    path('post-activity', views.submit_kegiatan),
    path('post-participant/<int:id_kegiatan>', views.submit_peserta),
]