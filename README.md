# Story 3 - 6

[![pipeline status](https://gitlab.com/amalias.annisa/story-4-ppw/badges/master/pipeline.svg)](https://gitlab.com/amalias.annisa/story-4-ppw/-/commits/master)


This repository contains of my PPW assignments from story 3 to 6.
- Name  : Annisa Amalia Sholeka
- NPM   : 1906354002


## Link

https://annisa-amalia.herokuapp.com

## License

This template was cloned from [here](https://github.com/laymonage/django-template-heroku).