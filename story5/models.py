from django.db import models

# Create your models here.
class MataKuliah(models.Model):

    subject = models.CharField(max_length=100)
    lecture = models.CharField(max_length=100)
    credits = models.CharField(max_length=100)
    desc = models.CharField(max_length=2000)
    terms = models.CharField(max_length=100)
    location = models.CharField(max_length=200)

    def __str__(self):
        return self.subject