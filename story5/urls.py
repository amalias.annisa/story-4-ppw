from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.formStory5),
    # path('post', views.postStory5),
    path('delete/<str:nama_matkul>', views.deleteData),
    path('<str:nama_matkul>', views.detail_jadwal),
]
