from django import forms
from .models import MataKuliah

years = [x for x in range(2019, 2025)]

class formMatkul(forms.Form):

    lecture = forms.CharField(
        label = "Lecture",
        max_length = 100,
        required = True,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm', 'placeholder': 'Pak Adin'})
    )

    subject = forms.ChoiceField(
        label = 'Subject',
        choices=[
                ('PPW', 'PPW'),
                ('SDA', 'SDA'),
                ('Adbis', 'Adbis'),
                ('PPM', 'PPM'),
                ('DDAK','DDAK'),
                ('Agama','Agama'),
                ('Others', 'Others')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'PPW'})
    )

    terms = forms.ChoiceField(
        label = 'Category',
        choices=[('2019/2020', '2019/2020'),
                ('2020/2021', '2019/2020'),
                ('2021/2022', '2019/2020'),
                ('2022/2023', '2019/2020')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'})
    )

    credits = forms.CharField(
        label = "Lecture",
        max_length = 100,
        required = True,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm', 'placeholder': '3 SKS'})
    )

    desc = forms.CharField(
        label = "desc",
        max_length = 100,
        required = True,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm'})
    )

    location = forms.CharField(
        label = "location",
        max_length = 100,
        required = False,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm'})
    )