from django.shortcuts import render, redirect

from django import forms
# Create your views here.
from django.http import HttpResponse
from story5.models import MataKuliah
from .forms import formMatkul
from .models import MataKuliah

# Create your views here.
def formStory5(request):
    schedule= MataKuliah.objects.all()
    form= formMatkul(request.POST or None)
    schedule_context = {
        'schedule' : schedule,
        'form' : form,
    }
    if request.method == "POST":
        if form.is_valid():
            schedule.create(
                subject = form.cleaned_data.get('subject'),
                lecture = form.cleaned_data.get('lecture'),
                credits = form.cleaned_data.get('credits'),
                desc = form.cleaned_data.get('desc'),
                terms = form.cleaned_data.get('terms'),
                location = form.cleaned_data.get('location'),
                )

    return render(request, 'story5.html', schedule_context)

def story5(request):
    form_tambah = formMatkul()
    data = MataKuliah.objects.all()
    response = {
        'form_tambah': form_tambah,
        'data': data,
    }
    return render(request, 'story5.html', response)

def detail_jadwal(request, nama_matkul):
    data = MataKuliah.objects.filter(subject=nama_matkul)
    response = {
        'schedule': data,
    }
    return render(request, 'story5details.html', response)

def deleteData(request, nama_matkul):
    print(nama_matkul)
    data = MataKuliah.objects.filter(subject=nama_matkul)
    data.delete()
    return redirect('/story5')